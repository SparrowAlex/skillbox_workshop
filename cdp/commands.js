class Commands {
    get professionBlockLocator() { return $('.//h3[contains(text(), "Профессии")]/following-sibling::a/span'); }

    get professionsButtonNext() { return $('section.courses-block > button.courses-block__load'); }

    async waitForResponse(url, body) {
        const puppeteer = await browser.getPuppeteer();
        const page = (await puppeteer.pages())[0];
        await page.waitForResponse(
            url = 'https://skillbox.ru/api/v6/ru/sales/skillbox/directions/all/nomenclature/profession/?page=1&limit=10&type=profession',
            body = {
                meta: [{
                    total: 162,
                }],
            },
        );
    }

    async checkProfessionCount(expectedCount) {
        const component = await this.professionBlockLocator;
        const text = await component.getText();
        expect(text).toContain(expectedCount);
    }

    async checkProfessionsButtonText(expectedText) {
        const button = await this.professionsButtonNext;
        const button_text = await button.getText();
        expect(button_text).toContain(expectedText);
    }

    async waitForArticleCount(expectedCount) {
        await browser.waitUntil(async () => {
            const articles = await $$('article.card');
            return articles.length === expectedCount;
        });
    }
}

module.exports = new Commands();
